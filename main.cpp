//��� �������� ������ ����� ������ ������� ��������� ������ ������
//����� ����� �����: ��������� �� ���� �������� ����� ������ ����:
//1.��� ������� ������� ����� AAT 0.0 (����, ��������� � �����, ��� � ������ ����, � ������� ��� �� �������������)
//2.��� ����� ����� ��������� ��������.

//��� ����������� ������������� ����� ������������ ���������������� ������� ���������, � ������� ������ 0, 1 � -1 ��
//i j ����� ����� ��� �����, ������������ i � j �������. �� ���������: 1 ���� ������� - �����, -1 - ���� ���� � 0 � ��������� ������
//���� ������ �������� � ������� del_node, �������� �������� RAT,AAT � slacks � �������������� ��������� ��������.
//���������� ���� ������� ��������� ���������� ���� adjacency_matrix � �.-��� main ���� adj_matrix �� ���� ���������.

#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

//
//AAT �������� ���������� RAT, ��� ��� ������� ������ ������ RAT-�������
//
void AAT(float* AAT_matrix, float** adj_matrix, vector<float> del_node, int size, int i)
{
    float tmp = 0, max = 0;
    int j = 0;
    int k = 0;
    if(adj_matrix[i][i] == -1){
        AAT_matrix[i] = 0;
        return;
    }
    for(j = 0; j < size; j++){
        tmp = 0;
        if(adj_matrix[i][j] < 0){
           if(AAT_matrix[j] == -1)
               AAT(AAT_matrix, adj_matrix, del_node, size, j);
           tmp = AAT_matrix[j] + del_node[i] - adj_matrix[i][j];
           if(tmp > max){
               max = tmp;
               k = j;
               }
        }
    }
    AAT_matrix[i] = max;

}
//
//������� RAT ��� ���� ������
//������� �������� - �������� �� ������� �������, ����� ������ ������ �� ����(������ ��� ���� �� �����)
//����� ���� �� ���� ��������� �������� � ��������� ���� �� ����� �� ����� �������
//
void RAT(float* RAT_matrix, float** adj_matrix, vector<float> del_node, int size, int i)
{
    float tmp = 0, max = 0;
    int j = 0;
    if(adj_matrix[i][i] == 1){
        return;
    }

    for(j = 0; j < size; j++){
        if(adj_matrix[i][j] > 0){//���� �� ����� �� ����� ������� � �������
            tmp = 0;
            if(RAT_matrix[j] == -1)// ���� �� ���������� �������� RAT ��� ������� �������, �� ����������
                RAT(RAT_matrix, adj_matrix, del_node, size, j);
            tmp = RAT_matrix[j] - del_node[j] - adj_matrix[i][j];
            if(tmp > max)
                max = tmp;
        }
    }
    RAT_matrix[i] = max;

}
//
//������� �������� �����������
//
int slacks(float* slack, float* RAT, float* AAT, int size)
{
    int i = 0;
    int k = 0;
    for(i = 0; i < size; i++){
        slack[i] = RAT[i] - AAT[i];
        if(slack[i] < 0)
            k = 1;
        }
    return k;
}

//
//������� ������� ��������� �������� �� ������ ������� ������� ��� ������
//���� ����� - ����� "1" � ���������, ���� - "-1"
//
void analysis(float** adj_matrix, int size)
{
    int i, j;
    int in_count, out_count;
    for(i = 0; i < size; i++){
        in_count = 0;
        out_count = 0;
        for(j = 0; j < size; j++){
            if(adj_matrix[i][j] > 0)
                out_count++;
            else if(adj_matrix[i][j] < 0)
                in_count++;
        }
        if(in_count == 0)
            adj_matrix[i][i] = -1;
        else if(out_count == 0)
            adj_matrix[i][i] = 1;

    }
}

int main()
{
    int vertex_count = 0;
    int i = 0, j = 0;
    float ch = 1;
    float* RAT_matrix;
    float* AAT_matrix;
    float* slack;
    vector<float> del_node; // ������ �������� ������
    FILE *delay_nodes, *delay_edges, *RAT_file;
    FILE *slacks_file, *result_file;
    int super_key = 0;//���� - ��������� ������� ���������������� ������


    //������� ����� �������� ������

    //printf("Its a delay nodes! \n");
    delay_nodes = fopen(".nodes", "r");
    while(fscanf(delay_nodes, "%f", &ch) != EOF){
        vertex_count++;
        del_node.push_back(ch);
    }
    fclose(delay_nodes);


    RAT_matrix = new float[vertex_count];
    AAT_matrix = new float[vertex_count];
    slack = new float[vertex_count];
    //������������� ������� ���������, RAT � AAT ������
    float** adjacency_matrix = new float*[vertex_count];
    for(i = 0; i < vertex_count; i++){
        RAT_matrix[i] = -1;
        AAT_matrix[i] = -1;
        adjacency_matrix[i] = new float[vertex_count];
        for(j = 0; j < vertex_count; j++)
            adjacency_matrix[i][j] = 0.0;
    }



    //������� ����� �������� �����
    delay_edges = fopen(".nets", "r");
    ch = 1;
    i = 0;
    j = 0;
    while(fscanf(delay_edges, "%d%d%f\n", &i, &j, &ch) != EOF){
        adjacency_matrix[i][j] = ch;
        ch *= -1;
        adjacency_matrix[j][i] = ch;
    }
    fclose(delay_edges);

    //������� RAT �����
    ch = 1;
    RAT_file = fopen(".aux", "r");
    while(fscanf(RAT_file, "%d%f\n", &i, &ch) != EOF){
        RAT_matrix[i] = ch;
    }
    fclose(RAT_file);

    //��������

    printf("\n\n");
    analysis(adjacency_matrix, vertex_count);

    for(i = 0; i < vertex_count; i++){
        if(RAT_matrix[i] == -1)
            RAT(RAT_matrix, adjacency_matrix, del_node, vertex_count, i);
        if(AAT_matrix[i] == -1)
            AAT(AAT_matrix, adjacency_matrix, del_node, vertex_count, i);
    }
    super_key = slacks(slack, RAT_matrix, AAT_matrix, vertex_count);

    slacks_file = fopen(".slacks", "w");
    result_file = fopen(".result", "w");
    fprintf(result_file, "%d\n", super_key);
    for(i = 0; i < vertex_count; i++){
        fprintf(slacks_file, "%f\n", slack[i]);
        if(slack[i] < 0)
            fprintf(result_file, "%d", i);
    }
    fclose(slacks_file);
    fclose(result_file);


    delete[]RAT_matrix;
    delete[]AAT_matrix;
    delete[]slack;
    //�������� ������� ���������
    for (i = 0; i < vertex_count; i++)
        delete[]adjacency_matrix[i];
    delete[]adjacency_matrix ;
    return 0;
}
