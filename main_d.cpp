#include <iostream>
#include <cstdlib>
#include <vector>
#include <cstdio>

#define VERTEX_MAX_DELAY 100
#define EDGE_MAX_DELAY 100
#define OUT_MAX_RAT 1000

using namespace std;



int main()
{
    int i, j, k, vertex_count, edges_count;
    float** matrix;
    vector<float> vertex_delay;
    FILE* nodes, *nets, *rat;
    srand(0);

    cout<<"input number of vertexes and edges:\n";
    cin>>vertex_count>>edges_count;
    matrix = (float**)malloc(sizeof(float*) * vertex_count);
    for(i = 0; i < vertex_count; i++){
        matrix[i] = (float*)malloc(sizeof(float) * vertex_count);
        for(j = 0; j < vertex_count; j++){
            matrix[i][j] = 0.0;
        }
    }
    for(i = 0; i < vertex_count; i++){
        vertex_delay.push_back((float)(rand() % VERTEX_MAX_DELAY));

        matrix[i][i] = rand() % 3 - 1;
    }

    for(i = 0; i < edges_count; i++){
        j = rand() % vertex_count; //����� ������������ ������
        if(matrix[j][j] != -1){//���������, �������� ������� � ���� ������� - ������
            while((k = rand() % vertex_count) && (k == j)){} //�������� ��������� ������ �������, ���������� �� ������
            if(matrix[j][k] != 0){  //���� � ������� ��� ���-�� ��������, �������� ��� ���������� ��������...
                i--;
                continue;
            }
            matrix[j][k] = (float)(rand() % EDGE_MAX_DELAY) / 10.0; //���! �����!
            matrix[k][j] = - matrix[j][k]; //� ����� �������!
            }
        else
            i--;
    }
    nodes = fopen(".nodes", "w");
    for(i = 0; i < vertex_count; i++)
        fprintf(nodes, "%f\n", vertex_delay[i]);
    fclose(nodes);

    nets = fopen(".nets", "w");
    for(i = 0; i < vertex_count; i++)
        for(j = 0; j < vertex_count; j++)
            if(matrix[i][j] > 0 && i != j)
                fprintf(nets, "%d %d %f\n", i, j, matrix[i][j]);
    fclose(nets);

    rat = fopen(".RAT", "w");
    for(i = 0; i < vertex_count; i++)
        if(matrix[i][i] == 1)
            fprintf(rat, "%d %f\n", i, (float)(rand() % OUT_MAX_RAT) / 10.0);
    fclose(rat);

    return 0;
}
